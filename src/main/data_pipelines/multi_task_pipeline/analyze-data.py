# Databricks notebook source
print('Analyzing data...')

# COMMAND ----------

from pyspark.sql.functions import current_timestamp
yellowTaxi = (spark.readStream
                  .format("delta")
                  .option("maxBytesPerTrigger", str(1024 * 1024 * 4))
                  .load("/databricks-datasets/nyctaxi/tables/nyctaxi_yellow")
                  .withColumn("current_timestamp", current_timestamp()))

# COMMAND ----------


