# Databricks notebook source
print('Loading yellow taxi dataset...')

# COMMAND ----------

yellowTaxi = (spark.readStream
                  .format("delta")
                  .option("maxBytesPerTrigger", str(1024 * 1024 * 4))
                  .load("/databricks-datasets/nyctaxi/tables/nyctaxi_yellow"))

# COMMAND ----------

display(yellowTaxi)
