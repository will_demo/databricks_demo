# Databricks Demo Using GitLab
This repo contains commonly artifacts used to interact with a Databricks workspace.

## Code Artifacts
- Notebooks - a web-based interface to a document that contains runnable code, visualizations, and narrative text
- Python File - a collection of commands in a file designed to be executed like a program
- Python Binary - wheel/egg files that contain pre-built Python libraries that can be attached to a Databricks cluster

## Infrastructure Artifacts
- Terraform - an infrastructure as code (IaC) scripts that allows you to build, change, and version Databricks infrastructure using the [Databricks Terraform provider](https://registry.terraform.io/providers/databrickslabs/databricks/latest/docs)
- Python Scripts - scripts that directly consume the Databricks APIs in order to deploy and version Databricks infrastructure
