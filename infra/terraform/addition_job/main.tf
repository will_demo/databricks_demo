terraform {
  required_providers {
    databricks = {
      source = "databrickslabs/databricks"
      version = "0.3.5"
    }
  }
}

provider "databricks" {
  host  = "https://<workspace_url_goes_here>"
  token = "<dapi_token_goes_here>"
}

resource "databricks_notebook" "addition_notebook" {
  path     = "/Users/will.girten@databricks.com/TerraformDemo/Addition"
  language = "PYTHON"
  content_base64 = base64encode(<<-EOT
    i = 1
    while ( i < 25):
      i = i + 1
    print(i)
    EOT
  )
}

resource "databricks_job" "addition_job" {
  name = "AdditionJob"
  new_cluster {
    spark_version = "8.3.x-cpu-ml-scala2.12"
    driver_node_type_id = "i3.xlarge"
    node_type_id = "i3.xlarge"
    autoscale {
       min_workers = 1
       max_workers = 3
    }
    spark_conf = {
       "spark.databricks.delta.preview.enabled": true
    }
    custom_tags = {
       "JobDesc": "Sums integers from 1-25"
    }
  }
  notebook_task {
    notebook_path = databricks_notebook.addition_notebook.path
  }
  schedule {
    quartz_cron_expression = "0 0 12 * * ?"
    timezone_id = "America/New_York"
  }
}

output "job_id" {
  value = databricks_job.addition_job.id
}
