terraform {
  required_providers {
    databricks = {
      source = "databrickslabs/databricks"
      version = "0.3.5"
    }
  }
}

provider "databricks" {
  host  = "https://<workspace_url_goes_here>"
  token = "<dapi_token_goes_here>"
}

resource "databricks_notebook" "notebook_inline" {
  path     = "/Users/will.girten@databricks.com/TerraformDemo/HelloWorld"
  language = "PYTHON"
  content_base64 = base64encode(<<-EOT
    print(f'Hello, World!')
    EOT
  )
}

output "notebook_url" {
  value = databricks_notebook.notebook_inline.url
}
