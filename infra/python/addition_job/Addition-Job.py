# Databricks notebook source
import requests
import base64


WORKSPACE_URL = 'https://<workspace_url_goes_here>'
DB_API_TOKEN = '<dapi_token_goes_here>'


# Notebook definition
notebook_def_json = {
  "path": "/Users/will.girten@databricks.com/TerraformDemo/Addition_Job_Py",
  "language": "PYTHON",
  "content": base64.b64encode("""
    i = 1
    while ( i < 25):
      i = i + 1
    print(i)
  """.encode('ascii')),
  "overwrite": True
}


def import_notebook(workspace_url, api_token, notebook_def):
  """Imports a notebook into a target Databricks workspace."""
  response = requests.post(
    workspace_url + 'api/2.0/workspace/import',
    headers = {
      'Authorization': 'Bearer ' + api_token
    },
    json = notebook_def
  )
  return response.status_code


import_notebook(WORKSPACE_URL, DB_API_TOKEN, notebook_def_json)


# Job definition
job_def_json = {
  "name": "Addition Job Py",
  "new_cluster": {
    "spark_version": "7.3.x-scala2.12",
    "node_type_id": "i3.xlarge",
    "aws_attributes": {
      "availability": "ON_DEMAND"
    },
    "num_workers": 2
  },
  "timeout_seconds": 3600,
  "max_retries": 1,
  "schedule": {
    "quartz_cron_expression": "0 0 12 * * ?",
    "timezone_id": "America/New_York"
  },
  "notebook_path": "/Users/will.girten@databricks.com/TerraformDemo/Addition_Job_Py"
}


def create_job(workspace_url, api_token, job_def):
  """Creates a Databricks Job given a workspace URL and Job Definition (as JSON)"""
  response = requests.post(
    workspace_url + 'api/2.0/jobs/create',
    headers = {
      'Authorization': 'Bearer ' + api_token
    },
    json=job_def
  )
  return response.status_code


create_job(WORKSPACE_URL, DB_API_TOKEN, job_def_json)
