import requests


WORKSPACE_URL = 'https://<workspace_url_goes_here>'
DB_API_TOKEN = '<dapi_token_goes_here>'


multi_task_job_def = {
  "name": "My Multi-Task Job",
  "max_concurrent_runs": 1,
  "tasks": [
    {
      "task_key": "clean_data",
      "description": "Clean and prepare the data",
      "notebook_task": {
        "notebook_path": "/Users/will.girten@databricks.com/TerraformDemo/MultiTaskJobs/clean-data"
      },
      "new_cluster": {
        "spark_version": "7.3.x-scala2.12",
        "node_type_id": "i3.xlarge",
        "aws_attributes": {
          "availability": "ON_DEMAND"
        },
        "num_workers": 2
      },
      "timeout_seconds": 3600,
      "max_retries": 3,
      "retry_on_timeout": True
    },
    {
      "task_key": "analyze_data",
      "description": "Perform an analysis of the data",
      "notebook_task": {
        "notebook_path": "/Users/will.girten@databricks.com/TerraformDemo/MultiTaskJobs/analyze-data"
      },
      "depends_on": [
        {
          "task_key": "clean_data"
        }
      ],
      "new_cluster": {
        "spark_version": "7.3.x-scala2.12",
        "node_type_id": "i3.xlarge",
        "aws_attributes": {
          "availability": "ON_DEMAND"
        },
        "num_workers": 2
      },
      "timeout_seconds": 3600,
      "max_retries": 3,
      "retry_on_timeout": True
    },
    {
      "task_key": "report_data",
      "description": "Building report of data",
      "notebook_task": {
        "notebook_path": "/Users/will.girten@databricks.com/TerraformDemo/MultiTaskJobs/report-data"
      },
      "depends_on": [
        {
          "task_key": "clean_data"
        }
      ],
      "new_cluster": {
        "spark_version": "7.3.x-scala2.12",
        "node_type_id": "i3.xlarge",
        "aws_attributes": {
          "availability": "ON_DEMAND"
        },
        "num_workers": 2
      },
      "timeout_seconds": 3600,
      "max_retries": 3,
      "retry_on_timeout": True
    }
  ],
  "schedule": {
    "quartz_cron_expression": "0 0 12 * * ?",
    "timezone_id": "America/New_York"
  }
}


def create_job(workspace_url, api_token, job_def):
  """Creates a multitask job defintion given a Databricks Workspace URL"""
  response = requests.post(
    workspace_url + '/2.0/jobs/create',
    headers={
      'Authentication': api_token
    },
    json=job_def
  )
  return response.status_code


create_job(WORKSPACE_URL, DB_API_TOKEN, multi_task_job_def)
